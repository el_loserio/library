'''
See LICENCE_BSD for licensing information

@author: spd
'''

def write(out_file, value, sep=',', na='', quote=''):
    if value is None:
        out_file.write(na)
    else:
        value = str(value)
        value = value.replace(',', '_')
        value = value.replace('\\', '\\\\')
        value = value.replace('\n', '\\n')
        value = value.replace('\r', '\\r')
        value = value.replace('\t', '\\t')
        value = value.replace('"', ' ')
        value = value.replace('\'', ' ')
        value = quote + value + quote
        out_file.write(value)
    if sep:
        out_file.write(sep)
        
def end_line(out_file):
    out_file.write('\n')
